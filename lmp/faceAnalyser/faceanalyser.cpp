/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/

#include "faceanalyser.h"

faceAnalyser::faceAnalyser()
{
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */

void faceAnalyser::createVector(vector<vector<double> > facialMotion, Mat flow, vector<FaceROI> meshs, Mat frame)
{
    //nothing
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */

/**
 * @brief return the mouvement vector
 * @return
 */

vector< vector<double> > faceAnalyser::getVector()
{
    return _meanVectorSequence;
}
