/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/

#ifndef OURANALYSER_H
#define OURANALYSER_H

#include "faceanalyser.h"

class ourAnalyser : public faceAnalyser
{
public:
    ourAnalyser();


    /**
     * @brief cumulate the lmp vectors
     */
    void createVectorCumul(vector< vector<double> > cumul);

    /**
     * @brief save the GMD in a file
     * @param emotion (int) : emotion label
     * @param name (string) : path to the file
     * @param subjectNumber (int) : subject name
     */
    void saveAggregatedOccurences(int emotion, string name, int subjectNumber);

    /**
     * @brief initialize the size of the mouvement vector
     * @param bin (int) : number of the histogram bins
     * @param region (int) : number of facial regions
     */
    void initialize(int bin, int region);

private:
    vector< vector<double> > _histogram, _cumul;
};

#endif // OURANALYSER_H
