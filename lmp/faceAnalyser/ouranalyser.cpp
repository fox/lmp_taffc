/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/

#include "ouranalyser.h"

ourAnalyser::ourAnalyser()
{

}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */


void ourAnalyser::initialize(int bin, int region)
{
    for(int i = 0; i < region; i++)
    {
        vector<double> tmp(bin,0);
        _histogram.push_back(tmp);
        _cumul.push_back(tmp);
    }
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */



void ourAnalyser::createVectorCumul(vector< vector<double> > cumul)
{
    for(int i = 0; i < cumul.size(); i++)
    {
        for(int j = 0; j < cumul[i].size(); j++)
        {
            if(cumul[i][j] > 0)
            {
                _cumul[i][j] += cumul[i][j];
            }
        }
    }
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */


void ourAnalyser::saveAggregatedOccurences(int emotion, string name, int subjectNumber)
{
    stringstream C,N;
    N << subjectNumber;
    C << emotion;

    int val = 0;

    string filestream = C.str() + " ";

    for(int i = 0; i < _cumul.size(); i++)
    {
        for(int j = 0; j < _cumul[0].size(); j++)
        {
            val++;
            stringstream R1,V1;
            R1 << val;
            V1 << _cumul[i][j];

            filestream += R1.str() + ":" + V1.str() + " ";
        }
    }

    ofstream fichier(name.c_str(), ios::out | ios::app);
    if(fichier)
    {
        fichier << filestream + "\n";
        fichier.close();
    }
}
